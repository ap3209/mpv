;NSIS Modern User Interface
;Welcome/Finish Page Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

  !include "x64.nsh"
  !include "LogicLib.nsh"

  !include "FileAssociation.nsh"

  !include "FileFunc.nsh"

;--------------------------------
;General

  ;Name and file
  Name "mpv"
  OutFile "mpv x64.exe"
  Unicode True

  ;Default installation folder
  InstallDir "$PROGRAMFILES64\mpv"

  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\mpv" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

  SetCompressor /SOLID "lzma"

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

  !define MUI_ICON "C:\Users\tz2\Documents\mpv\mpvicon.ico"
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "C:\Users\tz2\Documents\mpv\mpvicon.bmp"
  !define MUI_HEADERIMAGE_RIGHT

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "C:\Users\tz2\Documents\mpv\LICENSE.GPL"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "Core Program" coreprogram

  SetOutPath "$INSTDIR"

  ;ADD YOUR OWN FILES HERE...
  File /r "C:\mpv\*"

  ;Store installation folder
  WriteRegStr HKCU "Software\mpv" "" $INSTDIR

  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2

  SetRegView "64"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "QuietUninstallString" "$INSTDIR\Uninstall.exe /S"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "DisplayName" "mpv"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "Publisher" "AP3209"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "DisplayIcon" "$INSTDIR\mpv.exe"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "DisplayVersion" "0.35.0"
  WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "NoModify" 1
  WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "NoRepair" 1
  WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv" "EstimatedSize" "$0"

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "Create desktop entry" desktop

  SetOutPath "$INSTDIR"

  ;ADD YOUR OWN FILES HERE...
  CreateShortcut "$desktop\mpv.lnk" "$instdir\mpv.exe"

  ;Store installation folder
  WriteRegStr HKCU "Software\mpv" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "Create StartMenu entry" startmenu

  SetOutPath "$INSTDIR"

  ;ADD YOUR OWN FILES HERE...
  CreateDirectory "$STARTMENU\mpv"
  CreateShortcut "$STARTMENU\mpv\mpv.lnk" "$instdir\mpv.exe"
  CreateShortcut "$STARTMENU\mpv\mpbindings.png.lnk" "$instdir\doc\mpbindings.png"
  CreateShortcut "$STARTMENU\mpv\Uninstall.lnk" "$instdir\Uninstall.exe"

  ;Store installation folder
  WriteRegStr HKCU "Software\mpv" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "MY CONFIG" config

  SetOutPath "$INSTDIR"

  ;ADD YOUR OWN FILES HERE...
  CreateDirectory "$APPDATA\mpv"
  CreateDirectory "$APPDATA\mpv\scripts"
  CreateDirectory "$APPDATA\mpv\watch_later"

  CopyFiles "$INSTDIR\customfile\mpv\input.conf" "$APPDATA\mpv"
  CopyFiles "$INSTDIR\customfile\mpv\mpv.conf" "$APPDATA\mpv"
  CopyFiles "$INSTDIR\customfile\mpv\scripts\mpv_thumbnail_script_client_osc.lua" "$APPDATA\mpv\scripts"
  CopyFiles "$INSTDIR\customfile\mpv\scripts\mpv_thumbnail_script_server.lua" "$APPDATA\mpv\scripts"

  ;Store installation folder
  WriteRegStr HKCU "Software\mpv" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

Section "set this app as a default player" formate

  SetOutPath "$INSTDIR"

  ;ADD YOUR OWN FILES HERE...
  ${registerExtension} "$instdir\mpv.exe" ".mkv" "MKV_File"
  ${unregisterExtension} ".mkv" "MKV File"

  ${registerExtension} "$instdir\mpv.exe" ".mp4" "MP4_File"
  ${unregisterExtension} ".mp4" "MP4 File"

  ${registerExtension} "$instdir\mpv.exe" ".mp3" "MP3_File"
  ${unregisterExtension} ".mp3" "MP3 File"

  ${registerExtension} "$instdir\mpv.exe" ".ac3" "AC3_File"
  ${unregisterExtension} ".ac3" "AC3 File"

  ${registerExtension} "$instdir\mpv.exe" ".a52" "A52_File"
  ${unregisterExtension} ".a52" "A52 File"

  ${registerExtension} "$instdir\mpv.exe" ".eac3" "EAC3_File"
  ${unregisterExtension} ".eac3" "EAC3 File"

  ;Store installation folder
  WriteRegStr HKCU "Software\mpv" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_coreprogram ${LANG_ENGLISH} "This is the actual core program and files."
  LangString DESC_desktop ${LANG_ENGLISH} "Create desktop shortcut for easy access by default enable."
  LangString DESC_startmenu ${LANG_ENGLISH} "Create startmenu shortcut for easy access by default enable."
  LangString DESC_config ${LANG_ENGLISH} "Custum config and Setting."
  LangString DESC_formate ${LANG_ENGLISH} "Set this app as a default player."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${coreprogram} $(DESC_coreprogram)
    !insertmacro MUI_DESCRIPTION_TEXT ${desktop} $(DESC_desktop)
    !insertmacro MUI_DESCRIPTION_TEXT ${startmenu} $(DESC_startmenu)
    !insertmacro MUI_DESCRIPTION_TEXT ${config} $(DESC_config)
    !insertmacro MUI_DESCRIPTION_TEXT ${formate} $(DESC_formate)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  ; Delete "$INSTDIR\*"

  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR"

  Delete "$desktop\mpv.lnk"

  RMDir /r "$STARTMENU\mpv"

  RMDir /r "$APPDATA\mpv"

  DeleteRegKey /ifempty HKCU "Software\mpv"

  SetRegView "64"
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\mpv"

SectionEnd