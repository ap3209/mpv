
# MPV

This repository files are use to build Windows installer of mpv.

To download mpv setup from [here](https://gitlab.com/ap3209/mpv/-/releases).

MPV setup is build using NSIS script with including my custom config and all mpv binaries are extract from MSYS2.

Official MPV page:- [Link](https://mpv.io/).

Note:- In mpv setup have option to deselect my config if you have your own config for mpv.![App Screenshot](https://gitlab.com/ap3209/mpv/-/raw/main/screenshot/Screenshot%202023-01-24%20003100.png)

